Tarantula Care and Husbandry
============================

This is a small eBook about Tarantula Care and Husbandry that anyone can revise, edit, et cetera!

Files included
--------------

Included in this repository is a .odt and .docx file for you to view and edit the document in your Office suite

And a .pdf file for you to view the eBook in a PDF viewer

So which merge requests get accepted?
-------------------------------------

If it's a revision or edit that I find interesting of course!

About the Author
----------------

I'd like to go by the name of "sysadmin" or "systadmin" in the internet.

I'm an enthusiast in tarantula care and Arachnology, at the time of writing I own one Brachypelma Smithi and one Lasiodora Parahybana tarantulas.

Aside from Arachnids, I like to code, specifically in Python and Haskell, I self-taught myself how to code.

I also like Linux and I do a bit of networking which is fun.

I like to play Guitar but someday I'd like to teach myself how to play a piano.

Also I support Free Software, so yeah, no to Proprietary Software

And don't forget to read the LICENSE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
